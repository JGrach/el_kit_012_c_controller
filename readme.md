## Introduce
C controller to El Kit 012
- Arduino Uno R3
- L298N 
- ultrasonic sensor
- line tracking
- servo motor

## Compilation
- Execute compil.sh
- televerse.sh to arduino televersement
- Found on https://balau82.wordpress.com/2011/03/29/programming-arduino-uno-in-pure-c/

## Tutorials
### AVR datasheet
- https://www.sparkfun.com/datasheets/Components/SMD/ATMega328.pdf

### PWM
- https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM
- https://sites.google.com/site/qeewiki/books/avr-guide/pwm-on-the-atmega328

### USART
- http://maxembedded.com/2013/09/the-usart-of-the-avr/

## Next Step
- refacto bluetooth
- find an efficient debug system when Rx Tx is used by bluetooth module
- speed handler
- infrared sensor