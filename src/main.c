#include <util/delay.h>
#include "Controllers/motor.h"
#include "Mapping/timer.h"
#include "Mapping/uart.h"

void program(Motors *left, Motors *right){
  while(1)
    {
        unsigned char a=uart_recieve();
        if(a == 'z' || a == 'e' || a == 'd'){
          motor_setDirection(left, FORWARD);
          left->velocity = a == 'd' ? 200 : 255;
        }
        if(a == 'z' || a == 'a' || a == 'q'){
          motor_setDirection(right, FORWARD);
          right->velocity = a == 'q' ? 200 : 255;
        }
        if(a == 'a' || a == 's'){
          left->velocity = 0;
        }
        if(a == 'e' || a == 's'){
          right->velocity = 0;
        }
        if(a == 'q' || a == 'x'){
          motor_setDirection(left, BACKWARD);
          left->velocity = a == 'q' ? 200 : 255;
        }
        if(a == 'd' || a == 'x'){
          motor_setDirection(right, BACKWARD);
          right->velocity = a == 'd' ? 200 : 255;
        }
        motor_update(left);
        motor_update(right);
    }
}

main(int argc, char const *argv[])
{
  Timers_options tOptions;
  tOptions.outputA = TIMER_OUTPUT_NONE_INVERTED;
  tOptions.outputB = TIMER_OUTPUT_NONE_INVERTED;
  tOptions.waveform = TIMER_WAVEFORM_FAST_DEFAULT;
  tOptions.prescale = 1;
  timerZeroInitializer(&tOptions);

  uart_options uOptions;
  uOptions.double_speed = DOUBLE_SPEED_FALSE;
  uOptions.baudrate = 9600;
  uOptions.dataBits = DATABITS_8;
  uOptions.receive = RECEIVE_TRUE;
  uOptions.transmit = TRANSMIT_TRUE;
  uart_init(&uOptions);

  Motors leftMotor = motorConstructor(7, 'D', 0, 'B', 5, 'D', 'B');
  Motors rightMotor = motorConstructor(3, 'B', 1, 'B', 6, 'D', 'A');

  program(&leftMotor, &rightMotor);
  return 0;
}
