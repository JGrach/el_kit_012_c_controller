#include "uart.h"

void baudrateCalculation(uint32_t baudrate, uint8_t double_speed) {
    uint16_t ubrr = 0;
    if (double_speed) {
        UCSR0A |= (1<<U2X0);
        ubrr = (F_CPU / (8UL * baudrate)) - 1;
    } else {
        ubrr = (F_CPU / (16UL * baudrate)) - 1;
    }
    UBRR0H = ubrr >> 8;
    UBRR0L = ubrr;
}

void uart_init(uart_options *options){
    baudrateCalculation(options->baudrate, options->double_speed);

    if(options->dataBits == 6 || options->dataBits == 8 || options->dataBits == 9) UCSR0C |= (1<<UCSZ00);
    if(options->dataBits == 7 || options->dataBits == 8 || options->dataBits == 9) UCSR0C |= (1<<UCSZ01);
    if(options->dataBits == 9) UCSR0C |= (1<<UCSZ02);

    if(options->parityBit){ 
        UCSR0C |= (1<<USBS0 | 1<<UPM01);
        if(options->parityOdd) UCSR0C |= (1<<UPM00);
    };

    if(options->receive) UCSR0B |= (1<<RXEN0);
    if(options->transmit) UCSR0B |= (1<<TXEN0);
}

void uart_transmit (unsigned char data){
    while (!(UCSR0A & (1<<UDRE0)));
    UDR0 = data;
}

unsigned char uart_recieve (void){
    while(!(UCSR0A & (1<<RXC0)));
    return UDR0;
}