#ifndef INPUT_H
#define INPUT_H

#include <avr/io.h>
#include "timer.h"

#define PIN_SET_ON(register, portMap) (register |= (1<<portMap))
#define PIN_SET_OFF(register, portMap) (register &= ~(1<<portMap))
 
#define OUTPUT 1
#define INPUT 0
#define HIGH 1
#define LOW 0

typedef struct InputPins InputPins;
struct InputPins {
  char registerMap;
  uint8_t portMap;
  char timerMap;
};

void setPinDirection(struct InputPins pin, uint8_t output);
void setDigitalWrite(struct InputPins pin, uint8_t high);
void setAnalogWrite(struct InputPins pin, uint8_t value);

#endif // INPUT_H