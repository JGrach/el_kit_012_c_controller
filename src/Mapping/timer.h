#ifndef TIMER_H
#define TIMER_H

#include <avr/io.h>

#define TIMER_WAVEFORM_DEFAULT 0
#define TIMER_WAVEFORM_FAST_DEFAULT 1
#define TIMER_WAVEFORM_CORRECTED_DEFAULT 2
#define TIMER_WAVEFORM_FAST_0A 3
#define TIMER_WAVEFORM_CORRECTED_0A 4

#define TIMER_OUTPUT_DISABLED 0
#define TIMER_OUTPUT_DEPENDS 1
#define TIMER_OUTPUT_NONE_INVERTED 2
#define TIMER_OUTPUT_INVERTED 3

#define SET_ON_TIMER(register, number) (register = number)

typedef struct Timers_options Timers_options;
struct Timers_options {
  uint8_t outputA;
  uint8_t outputB;
  uint8_t waveform;
  uint8_t prescale;
};

void timerZeroInitializer(Timers_options *options);

#endif // TIMER_H