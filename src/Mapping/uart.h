#ifndef UART_H
#define UART_H

#include <avr/io.h>

#define DOUBLE_SPEED_TRUE 1
#define DOUBLE_SPEED_FALSE 1
#define DATABITS_6 6
#define DATABITS_8 7
#define DATABITS_7 8
#define DATABITS_9 9
#define PARITY_BIT_TRUE 1
#define PARITY_BIT_FALSE 0
#define PARITY_ODD 1
#define PARITY_EVEN 0
#define TRANSMIT_TRUE 1
#define TRANSMIT_FALSE 1
#define RECEIVE_TRUE 1
#define RECEIVE_FALSE 0

typedef struct uart_options uart_options;
struct uart_options {
  uint32_t baudrate;
  uint8_t double_speed;
  uint8_t dataBits;
  uint8_t parityBit;
  uint8_t parityOdd;
  uint8_t receive;
  uint8_t transmit;
};

#endif // UART_H