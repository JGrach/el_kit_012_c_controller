#include "timer.h"

void timerZeroInitializer(Timers_options *options){
  if(options->outputA == TIMER_OUTPUT_DEPENDS || options->outputA == TIMER_OUTPUT_INVERTED) TCCR0A |= (1<<COM0A0);
  if(options->outputA == TIMER_OUTPUT_INVERTED || options->outputA == TIMER_OUTPUT_NONE_INVERTED) TCCR0A |= (1<<COM0A1);

  if(options->outputB == TIMER_OUTPUT_DEPENDS || options->outputB == TIMER_OUTPUT_INVERTED) TCCR0A |= (1<<COM0B0);
  if(options->outputB == TIMER_OUTPUT_INVERTED || options->outputB == TIMER_OUTPUT_NONE_INVERTED) TCCR0A |= (1<<COM0B1);

  if(options->waveform != TIMER_WAVEFORM_DEFAULT) TCCR0A |= (1<<WGM00);
  if(options->waveform == TIMER_WAVEFORM_FAST_DEFAULT || options->waveform == TIMER_WAVEFORM_FAST_0A) TCCR0A |= (1<<WGM01);
  if(options->waveform == TIMER_WAVEFORM_CORRECTED_0A || options->waveform == TIMER_WAVEFORM_FAST_0A) TCCR0B |= (1<<WGM02);

  if(options->prescale == 1 || options->prescale == 1024) TCCR0B |= (1<<CS00);
  if(options->prescale == 8 || options->prescale == 64) TCCR0B |= (1<<CS01);
  if(options->prescale == 64 || options->prescale == 1024) TCCR0B |= (1<<CS02);
}