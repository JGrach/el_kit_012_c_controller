#include "input.h"

void setPinDirection(struct InputPins pin, uint8_t output){
  switch(pin.registerMap){
    case 'B':
      if(output) PIN_SET_ON(DDRB, pin.portMap);
      else PIN_SET_OFF(DDRB, pin.portMap);
      break;
    case 'C':
      if(output) PIN_SET_ON(DDRC, pin.portMap);
      else PIN_SET_OFF(DDRC, pin.portMap);
      break;
    case 'D':
      if(output) PIN_SET_ON(DDRD, pin.portMap);
      else PIN_SET_OFF(DDRD, pin.portMap);
      break;
  }
}

void setDigitalWrite(struct InputPins pin, uint8_t high){
  switch(pin.registerMap){
    case 'B':
      if(high) PIN_SET_ON(PORTB, pin.portMap);
      else PIN_SET_OFF(PORTB, pin.portMap);
      break;
    case 'C':
      if(high) PIN_SET_ON(PORTC, pin.portMap);
      else PIN_SET_OFF(PORTC, pin.portMap);
      break;
    case 'D':
      if(high) PIN_SET_ON(PORTD, pin.portMap);
      else PIN_SET_OFF(PORTD, pin.portMap);
      break;
  }
}

void setAnalogWrite(struct InputPins pin, uint8_t value){
  if(pin.timerMap == 'A') SET_ON_TIMER(OCR0A, value);
  else if(pin.timerMap == 'B') SET_ON_TIMER(OCR0B, value);
}
