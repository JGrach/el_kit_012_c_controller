#ifndef DEBUG_H
#define DEBUG_H

#include <util/delay.h>
#include "../Mapping/input.h"

#define LED_DEBUG_INIT (DDRB |= (1<<5))
#define LED_DEBUG_ON (PORTB |= (1<<5))
#define LED_DEBUG_OFF (PORTB &= ~(1<<5))

void readByteDebug(uint8_t byteToRead);

#endif // DEBUG_H