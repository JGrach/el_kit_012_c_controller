#ifndef MOTOR_H
#define MOTOR_H

#include "../Mapping/input.h"

#define FORWARD 1
#define BACKWARD 0

typedef struct Motors Motors;
struct Motors {
  struct InputPins forward;
  struct InputPins backward;
  struct InputPins pwmDriver;
  uint8_t velocity;
  int direction;
};

Motors motorConstructor(
  uint8_t forwardPinMap, char forwardRegisterMap,
  uint8_t backwardPinMap, char backwardRegisterMap,
  uint8_t pwmDriverPinMap, char pwmDriverRegisterMap,
  char timerMap
);

void motor_update(Motors *motor);
void motor_setVelocity(Motors *motor, uint8_t velocity);
void motor_setDirection(Motors *motor, uint8_t direction);

#endif // MOTOR_H