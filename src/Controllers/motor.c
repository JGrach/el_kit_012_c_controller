#include "motor.h"

Motors motorConstructor(
    uint8_t forwardPinMap, char forwardRegisterMap,
    uint8_t backwardPinMap, char backwardRegisterMap,
    uint8_t pwmDriverPinMap, char pwmDriverRegisterMap,
    char timerMap
  ){
  InputPins forward;
  forward.registerMap = forwardRegisterMap;
  forward.portMap = forwardPinMap;

  InputPins backward;
  backward.registerMap = backwardRegisterMap;
  backward.portMap = backwardPinMap;

  InputPins pwmDriver;
  pwmDriver.registerMap = pwmDriverRegisterMap;
  pwmDriver.portMap = pwmDriverPinMap;
  pwmDriver.timerMap = timerMap;

  setPinDirection(forward, OUTPUT);
  setPinDirection(backward, OUTPUT);
  setPinDirection(pwmDriver, OUTPUT);

  Motors motor;
  motor.forward = forward;
  motor.backward = backward;
  motor.pwmDriver = pwmDriver;
  motor.direction = FORWARD;
  motor.velocity = 0;

  setDigitalWrite(motor.forward, HIGH);

  return motor;
}

void motor_update(Motors *motor){
  setAnalogWrite(motor->pwmDriver, motor->velocity);
}

void motor_setVelocity(Motors *motor, uint8_t velocity){
  motor->velocity = velocity;
}

void motor_setDirection(Motors *motor, uint8_t direction){
  motor->direction = direction;
  if(direction == FORWARD) {
    setDigitalWrite(motor->forward, HIGH);
    setDigitalWrite(motor->backward, LOW);
  } else if(direction == BACKWARD){
    setDigitalWrite(motor->backward, HIGH);
    setDigitalWrite(motor->forward, LOW);
  }
}
