#include "debug.h"

void readByteDebug(uint8_t byteToRead){
  LED_DEBUG_INIT;
  int i = 0;
  while(i<10){
    LED_DEBUG_OFF;
    _delay_ms(100);
    LED_DEBUG_ON;
    _delay_ms(100);
    i++;
  }
  LED_DEBUG_OFF;
  _delay_ms(1000);

  i = 0;
  while(i<8){
    if(byteToRead & (1<<i)){
      LED_DEBUG_ON;
      _delay_ms(1500);
      LED_DEBUG_OFF;
      _delay_ms(500);
    } else {
      LED_DEBUG_ON;
      _delay_ms(500);
      LED_DEBUG_OFF;
      _delay_ms(500);
    }
    i++;
  }
};