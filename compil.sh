#! /bin/bash

ROOT="$( cd "$(dirname "$0")" ; pwd -P )"
DIST="$ROOT/dist"
SRC="$ROOT/src"

if [ ! -d "$DIST" ]; then
  mkdir $DIST
fi

cd $DIST

C_FILES=$(find $SRC -type f -name "*.c")
avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c $(echo $C_FILES)

O_FILES=$(find $DIST -type f -name "*.o")
avr-gcc -mmcu=atmega328p $(echo $O_FILES) -o main
avr-objcopy -O ihex -R .eeprom main main.hex

rm $(echo $O_FILES)